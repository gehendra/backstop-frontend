/**
 * Created by gehendrakarmacharya on 6/1/17.
 */

'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');

var input = 'app/scss/general.scss';
var output = 'app/css';


gulp.task('sass', function() {
  return gulp.src('./app/scss/general.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'));
});

gulp.task('minify-css', function() {
  return gulp.src('./app/css/general.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./dist'));
});

gulp.task('default', function() {
 gulp.watch('./app/scss/*.scss', ['sass', 'minify-css']);
});
