"use strict";
(function(){
  var mobile_icon = document.querySelector('.mobile-nav-button');
  var mobile_nav = document.querySelector('.mobile-menu');
  var mobile_close = document.querySelector('.close-icon');
  mobile_icon.addEventListener('click', function() {
    mobile_nav.style.display = 'block';
    mobile_nav.style.height = '100%';
    console.log('hey you just click icon');
  });
  mobile_close.addEventListener('click', function() {
    mobile_nav.style.height = '0%';
  });
})();
